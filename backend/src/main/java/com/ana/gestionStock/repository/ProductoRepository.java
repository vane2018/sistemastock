package com.ana.gestionStock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ana.gestionStock.enty.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto,Integer> {
//String findbyNombre(String terrabusi);
}
