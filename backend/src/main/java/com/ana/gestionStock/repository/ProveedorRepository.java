package com.ana.gestionStock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ana.gestionStock.enty.Proveedor;

@Repository
public interface ProveedorRepository extends JpaRepository<Proveedor,Integer> {

}
