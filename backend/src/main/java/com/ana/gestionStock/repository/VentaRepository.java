package com.ana.gestionStock.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ana.gestionStock.enty.Venta;


@Repository
public interface VentaRepository extends JpaRepository<Venta,Integer>{

	
}
