package com.ana.gestionStock.web.rest;

import java.security.Principal;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController

public class LoginResource {
	
	@GetMapping("/login")
	public String login(@RequestParam(value="error",required=false)String error,
			@RequestParam(value="logout",required=false)String logout,
			Model model,Principal principal,RedirectAttributes flash) {
		
			if(principal !=null) {
				
				flash.addFlashAttribute("info", "Ya ha iniciado sesion anteriormente");
				return "redirect: /";
			}
			if(error != null) {
				model.addAttribute("error", "Error en el login:nombr ed eusuario o contraseña incorrecta,vuelva a intentarlo");
			}
			
			if(logout != null) {
				model.addAttribute("succes", "has cerrado sesion con exito");
			}
			return "login";
		
		
		

	}
}
