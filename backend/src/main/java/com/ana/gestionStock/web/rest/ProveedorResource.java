package com.ana.gestionStock.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ana.gestionStock.enty.Proveedor;

import com.ana.gestionStock.service.ProveedorService;
import com.ana.gestionStock.web.rest.util.HeaderUtil;
import com.ana.gestionStock.web.rest.util.ResponseUtil;


@RestController
@RequestMapping("/api")
public class ProveedorResource {

	
	
private final Logger log = LoggerFactory.getLogger(ProveedorResource.class);
	
	private static final String ENTITY_NAME="proveedor";
	
	private final ProveedorService proveedorService;

	

	public ProveedorResource(ProveedorService proveedorService) {
		
		this.proveedorService = proveedorService;
	}


	@GetMapping("/proveedores")
	public ResponseEntity<List<Proveedor>> getAllProveedor(){
		log.debug("Rest request to get a page of proveedor");
		List<Proveedor> page = proveedorService.findAll();
		return new ResponseEntity<>(page,HttpStatus.OK);
	}
	
	
	@PostMapping("/proveedores")
	public ResponseEntity <Proveedor> createProveedor(@RequestBody Proveedor proveedor) throws URISyntaxException{
		  log.debug("REST request to save Proveedor : {}", proveedor);

		    Proveedor result = proveedorService.save(proveedor);
		    return ResponseEntity.created(new URI("/api/proveedores/" + result.getIdProveedor()))
		        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProveedor().toString()))
		        .body(result);
	}
	
	
	  @PutMapping("/proveedores")
	  public ResponseEntity<Proveedor> updateProveedores(@RequestBody Proveedor proveedor) throws URISyntaxException {
	    log.debug("REST request to update Proveedor : {}", proveedor);

	    Proveedor result = proveedorService.save(proveedor);
	    return ResponseEntity.ok()
	        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, proveedor.getIdProveedor().toString()))
	        .body(result);
	  }
	
	  @GetMapping("/proveedores/{proveedorId}")
	  public ResponseEntity<Proveedor> getProveedor(@PathVariable Integer proveedorId) {
	    log.debug("REST request to get Proveedor : {}", proveedorId);
	    Optional<Proveedor> proveedorDTO = proveedorService.findOne(proveedorId);
	    return ResponseUtil.wrapOrNotFound(proveedorDTO);
	   // return ResponseUtil.wrapOrNotFound(productoDTO);
	  }

	  @DeleteMapping("/proveedores/{proveedorId}")
	  public ResponseEntity<Void> deleteProveedor(@PathVariable Integer proveedorId) {
	    log.debug("REST request to delete Provedoor : {}", proveedorId);
	    proveedorService.delete(proveedorId);
	    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, proveedorId.toString())).build();
	  }
	  
	
}
