package com.ana.gestionStock.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ana.gestionStock.enty.Producto;
import com.ana.gestionStock.service.ProductoService;
import com.ana.gestionStock.web.rest.util.HeaderUtil;
import com.ana.gestionStock.web.rest.util.ResponseUtil;


@RestController
@RequestMapping("/api")
public class ProductoResource {
	
	private final Logger log = LoggerFactory.getLogger(ProductoResource.class);
	
	private static final String ENTITY_NAME="producto";
	
	private final ProductoService productoService;

	public ProductoResource(ProductoService productoService) {
		super();
		this.productoService = productoService;
	}

	@GetMapping("/productos")
	public ResponseEntity<List<Producto>> getAllProducto(){
		log.debug("Rest request to get a page of producto");
		List<Producto> page = productoService.findAll();
		return new ResponseEntity<>(page,HttpStatus.OK);
	}
	
	
	@PostMapping("/productos")
	public ResponseEntity <Producto> createProducto(@RequestBody Producto producto) throws URISyntaxException{
		  log.debug("REST request to save Producto : {}", producto);

		    Producto result = productoService.save(producto);
		    return ResponseEntity.created(new URI("/api/productos/" + result.getIdProducto()))
		        .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getIdProducto().toString()))
		        .body(result);
	}
	
	
	  @PutMapping("/productos")
	  public ResponseEntity<Producto> updateProducto(@RequestBody Producto producto) throws URISyntaxException {
	    log.debug("REST request to update Producto : {}", producto);

	    Producto result = productoService.save(producto);
	    return ResponseEntity.ok()
	        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, producto.getIdProducto().toString()))
	        .body(result);
	  }
	
	  @GetMapping("/productos/{productoId}")
	  public ResponseEntity<Producto> getProducto(@PathVariable Integer productoId) {
	    log.debug("REST request to get Producto : {}", productoId);
	    Optional<Producto> productoDTO = productoService.findOne(productoId);
	    return ResponseUtil.wrapOrNotFound(productoDTO);
	   // return ResponseUtil.wrapOrNotFound(productoDTO);
	  }

	  @DeleteMapping("/productos/{productoId}")
	  public ResponseEntity<Void> deleteProducto(@PathVariable Integer productoId) {
	    log.debug("REST request to delete Producto : {}", productoId);
	    productoService.delete(productoId);
	    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, productoId.toString())).build();
	  }
	  
	
	
}
