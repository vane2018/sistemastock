package com.ana.gestionStock.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ana.gestionStock.enty.Venta;
import com.ana.gestionStock.enty.Venta;
import com.ana.gestionStock.service.VentaService;
import com.ana.gestionStock.service.VentaService;
import com.ana.gestionStock.web.rest.util.HeaderUtil;
import com.ana.gestionStock.web.rest.util.ResponseUtil;


@RestController
@RequestMapping("/api")
public class VentaResource {

	

	
	
private final Logger log = LoggerFactory.getLogger(VentaResource.class);
	
	private static final String ENTITY_NAME="Venta";
	
	private final VentaService ventaService;

	
	public VentaResource(VentaService ventaService) {
		super();
		this.ventaService = ventaService;
	}


	@GetMapping("/ventas")
	public ResponseEntity<List<Venta>> getAllVenta(){
		log.debug("Rest request to get a page of Venta");
		List<Venta> page = ventaService.findAll();
		return new ResponseEntity<>(page,HttpStatus.OK);
	}
	
	
	@PostMapping("/ventas/{ProductoId}")
	public ResponseEntity <Venta> createVenta(@RequestBody Venta venta,@PathVariable Integer ProductoId) throws URISyntaxException{
		  log.debug("REST request to save Venta : {}", venta);

		    Venta result = ventaService.SaveNuevaVenta(venta,ProductoId);

		    return new ResponseEntity<>(result,result== null ? HttpStatus.NOT_FOUND:HttpStatus.OK);
	}
	
	
	  @PutMapping("/ventas")
	  public ResponseEntity<Venta> updateProducto(@RequestBody Venta venta) throws URISyntaxException {
	    log.debug("REST request to update Venta : {}", venta);

	    Venta result = ventaService.save(venta);
	    return ResponseEntity.ok()
	        .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, venta.getIdVenta().toString()))
	        .body(result);
	  }
	
	  @GetMapping("/ventas/{ventaId}")
	  public ResponseEntity<Venta> getVenta(@PathVariable Integer ventaId) {
	    log.debug("REST request to get Venta : {}", ventaId);
	    Optional<Venta> VentaDTO = ventaService.findOne(ventaId);
	    return ResponseUtil.wrapOrNotFound(VentaDTO);
	   // return ResponseUtil.wrapOrNotFound(productoDTO);
	  }

	  @DeleteMapping("/ventas/{ventasId}")
	  public ResponseEntity<Void> deleteVenta(@PathVariable Integer ventaId) {
	    log.debug("REST request to delete Provedoor : {}", ventaId);
	    ventaService.delete(ventaId);
	    return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, ventaId.toString())).build();
	  }
}
