package com.ana.gestionStock.enty;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="proveedor")
public class Proveedor {

	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)	
	private Integer idProveedor;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column(name="telefono")
	private String telefono;
	
	@Column(name="descripcion")
	private String descripcion;
	
//bidireccional
	@OneToMany(mappedBy="proveedor",targetEntity=Producto.class,fetch=FetchType.LAZY, cascade= CascadeType.ALL)
	List<Producto> productos;
	
	//unidireccional
//	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)
	//@JoinColumn(name="proveedor_id")
	//List<Producto> productos;
	
	
	

	public Proveedor() {
		
		this.productos= new ArrayList<Producto>();
	}

	public Integer getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}
	
	
	public void addProductos(Producto prod) {
		this.productos.add(prod);
	}
	
	
	
}
