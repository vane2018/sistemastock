package com.ana.gestionStock.enty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="producto")
public class Producto {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer idProducto;
	
	@Column(name="codigo")
	private String codigo;
	
   @Column(name="nombre")
	private String nombre;

   @Column(name="precio")
	private Double precio;
   
    @Column(name="stock")
	private  int Stock;
	
    
	@ManyToOne(fetch=FetchType.LAZY)
	//@JoinColumn(name="idProveedor")
	private Proveedor proveedor;
	
	//@ManyToOne(fetch=FetchType.LAZY)
	//@JoinColumn(name="idVenta")
    //private Venta venta;

	public Integer getIdProducto() {
		return idProducto;
	}


	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Double getPrecio() {
		return precio;
	}


	public void setPrecio(Double precio) {
		this.precio = precio;
	}


	public int getStock() {
		return Stock;
	}


	public void setStock(int stock) {
		Stock = stock;
	}


	
	
	
	
	

}
