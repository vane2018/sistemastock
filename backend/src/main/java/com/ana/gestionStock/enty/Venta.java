package com.ana.gestionStock.enty;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="venta")
public class Venta {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Integer idVenta;
	
	@Column(name="cantidad")
	private int cantidad;
	
	@Column(name="total")
	private Double total;
	
	
	//@OneToMany(mappedBy="venta",targetEntity=Producto.class,fetch=FetchType.LAZY, cascade= CascadeType.ALL)
	//@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL)

//	List<Producto> productos;
	
	@ManyToOne
	@JoinColumn(name="idProducto")
	private Producto producto;
	
	

	
	
	
	
	public Venta() {
	
		// TODO Auto-generated constructor stub
	}

	public Venta(Integer idVenta, int cantidad, Double total, Producto producto) {
	
		this.idVenta = idVenta;
		this.cantidad = cantidad;
		this.total = total;
		this.producto = producto;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Integer getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(Integer idVenta) {
		this.idVenta = idVenta;
	}

	

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}


	
	
	public void realizaVenta(double precio) {
		
	double	subtotal=0;
	subtotal=precio*cantidad;
		
	}

	
	
}
