package com.ana.gestionStock.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ana.gestionStock.enty.Producto;
import com.ana.gestionStock.enty.Venta;
import com.ana.gestionStock.repository.ProductoRepository;
import com.ana.gestionStock.repository.VentaRepository;
import com.ana.gestionStock.service.VentaService;

@Service
@Transactional
public class VentaServiceImpl implements VentaService {

	private final VentaRepository ventaRepository;
	
	private final ProductoRepository productoRepository;

	
	


	

	public VentaServiceImpl(VentaRepository ventaRepository, ProductoRepository productoRepository) {
		super();
		this.ventaRepository = ventaRepository;
		this.productoRepository = productoRepository;
	}

	@Override
	public Venta save(Venta ventas) {
		// TODO Auto-generated method stub
		return this.ventaRepository.save(ventas);
	}

	@Override
	public List<Venta> findAll() {
		// TODO Auto-generated method stub
		return this.ventaRepository.findAll();
	}

	@Override
	public Optional<Venta> findOne(Integer idVenta) {
		// TODO Auto-generated method stub
		return this.ventaRepository.findById(idVenta);
	}

	@Override
	public void delete(Integer idVenta) {
		// TODO Auto-generated method stub
		this.ventaRepository.deleteById(idVenta);
	}

	@Override
	public Venta SaveNuevaVenta(Venta venta, Integer idProducto) {
		// TODO Auto-generated method stub
		
		Optional<Producto> prod= productoRepository.findById(idProducto);
		//String  product=productoRepository.findbyNombre("terrabusi");
		Producto p=prod.get();
		
		if(p.getStock()>venta.getCantidad()) {
			
			p.setStock(p.getStock()-venta.getCantidad());
			productoRepository.save(p);
			venta.setProducto(p);
			venta.setTotal(venta.getCantidad()*p.getPrecio());
			return this.ventaRepository.save(venta);
			
			
		}else {
		
		
		return null;}
	}

	

	//@Override
	/*public void ControlStock(Venta venta) {
		// TODO Auto-generated method stub
	
		Optional<Producto> productoEnc= productoServiceImpl.findOne(venta.getProductos().g);
		//Producto prod = null;

	 //ProductoServiceImp productoImpl;
	
	 //prod.setStock(prod.getStock()-1);
	 
	
	}*/
	

}
