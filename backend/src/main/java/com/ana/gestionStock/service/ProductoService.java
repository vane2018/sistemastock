package com.ana.gestionStock.service;

import java.util.List;
import java.util.Optional;

import com.ana.gestionStock.enty.Producto;

public interface ProductoService {

	  Producto save(Producto producto);

	  List<Producto> findAll();

	  Optional<Producto> findOne(Integer idProducto);

	  void delete(Integer idProducto);
	
}
