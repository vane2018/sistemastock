package com.ana.gestionStock.service;

import java.util.List;
import java.util.Optional;

import com.ana.gestionStock.enty.Venta;

public interface VentaService {
	
	 Venta save(Venta ventas);

	  List<Venta> findAll();

	  Optional<Venta> findOne(Integer idVenta);

	  void delete(Integer idVenta);
	  
	 Venta SaveNuevaVenta(Venta venta, Integer idProducto );
	  
	  
}
