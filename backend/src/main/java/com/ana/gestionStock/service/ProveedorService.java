package com.ana.gestionStock.service;

import java.util.List;
import java.util.Optional;


import com.ana.gestionStock.enty.Proveedor;

public interface ProveedorService {
	 Proveedor save(Proveedor proveedor);

	  List<Proveedor> findAll();

	  Optional<Proveedor> findOne(Integer idProveedor);

	  void delete(Integer idProveedor);
}
