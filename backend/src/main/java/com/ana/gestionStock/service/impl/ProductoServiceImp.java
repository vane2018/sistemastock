package com.ana.gestionStock.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ana.gestionStock.enty.Producto;
import com.ana.gestionStock.repository.ProductoRepository;
import com.ana.gestionStock.service.ProductoService;

@Service
@Transactional
public class ProductoServiceImp  implements ProductoService{

	
	private final ProductoRepository productoRepository;
	
	
	public ProductoServiceImp(ProductoRepository productoRepository) {
		
		this.productoRepository = productoRepository;
	}

	@Override
	public Producto save(Producto producto) {
		// TODO Auto-generated method stub
		return this.productoRepository.save(producto);
	}

	@Override
	public List<Producto> findAll() {
		// TODO Auto-generated method stub
		return this.productoRepository.findAll();
	}

	@Override
	public Optional<Producto> findOne(Integer idProducto) {
		// TODO Auto-generated method stub
		return this.productoRepository.findById(idProducto);
	}

	@Override
	public void delete(Integer idProducto) {
		// TODO Auto-generated method stub
		this.productoRepository.deleteById(idProducto);
		
	}

}
