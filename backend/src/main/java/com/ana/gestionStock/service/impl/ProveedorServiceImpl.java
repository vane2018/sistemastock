package com.ana.gestionStock.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ana.gestionStock.repository.ProveedorRepository;
import com.ana.gestionStock.service.ProveedorService;
import com.ana.gestionStock.enty.Proveedor;



@Service
@Transactional
public class ProveedorServiceImpl implements ProveedorService{

	private final ProveedorRepository proveedorRepository;

	
	
	
	public ProveedorServiceImpl(ProveedorRepository proveedorRepository) {
		
		this.proveedorRepository = proveedorRepository;
	}

	@Override
	public Proveedor save(Proveedor proveedor) {
		// TODO Auto-generated method stub
		return this.proveedorRepository.save(proveedor);
	}

	@Override
	public List<Proveedor> findAll() {
		// TODO Auto-generated method stub
		return this.proveedorRepository.findAll();
	}

	@Override
	public Optional<Proveedor> findOne(Integer idProveedor) {
		// TODO Auto-generated method stub
		return this.proveedorRepository.findById(idProveedor);
	}

	@Override
	public void delete(Integer idProveedor) {
		// TODO Auto-generated method stub
		this.proveedorRepository.deleteById(idProveedor);
		
	}
	
	


}
